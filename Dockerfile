FROM node:latest AS builder
COPY package*.json /build/
WORKDIR /build/
RUN npm install
COPY . /build/
RUN npm run build

FROM nginx:latest
COPY --from=builder /build /usr/share/nginx/html
